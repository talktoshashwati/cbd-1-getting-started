ARG BASE_IMAGE
FROM $BASE_IMAGE

WORKDIR /

# RUN apt-get update
COPY . .

ENV PATH="venv/bin:$PATH"

EXPOSE 5000
CMD ["python", "-u", "main.py"]